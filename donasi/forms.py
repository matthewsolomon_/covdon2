from django import forms

class FormDonasi(forms.Form):
    Nama = forms.CharField(label='', widget=forms.TextInput(attrs={
        'style' : 'margin-bottom:15px;',
        'class' : 'form-control form-control-lg',
        'placeholder' : 'Nama',
        'type' : 'text',
        'required' : True
    }))
    Amount = forms.IntegerField(label='', widget=forms.TextInput(attrs={
        'style' : 'margin-bottom:15px;',
        'class' : 'form-control form-control-lg',
        'placeholder' : 'Amount',
        'type' : 'number',
        'min' : '0',
        'required' : True
    }))
    Message = forms.CharField(label='', widget=forms.Textarea(attrs={
        'class' : 'form-control form-control-lg',
        'rows' : '6',
        'placeholder' : 'Message',
        'type' : 'text',
        'required' : False
    }))

class FormKartu(forms.Form):
    CardNumber = forms.IntegerField(label='', widget=forms.TextInput(attrs={
        'style' : 'margin-bottom:25px',
        'class' : 'form-control form-control-lg',
        'placeholder' : 'Card Number',
        'type' : 'number',
        'min' : '0',
        'required' : True
    }))
    ValideDate = forms.CharField(label='', widget=forms.TextInput(attrs={
        'style' : 'margin-bottom:25px',
        'class' : 'form-control form-control-lg',
        'placeholder' : 'MM/YY',
        'type' : 'text',
        'required' : True
    }))
    Code = forms.IntegerField(label='', widget=forms.TextInput(attrs={
        'style' : 'margin-bottom:25px',
        'class' : 'form-control form-control-lg',
        'placeholder' : 'Code',
        'type' : 'number',
        'min' : '0',
        'required' : True
    }))
    