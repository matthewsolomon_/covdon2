# Generated by Django 3.1.3 on 2020-11-21 15:54

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('donasi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='Tanggal',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now),
        ),
    ]
