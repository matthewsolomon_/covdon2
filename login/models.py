from django.db import models
from django.contrib.auth.models import User

class Profs(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    full_name = models.CharField(max_length = 100)
    profile_picture_url = models.URLField(blank = True, default = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/6d6d42eb-ebd7-4f5e-93e0-df3302072682/de0bmry-1b2d9b6a-0134-4b07-96a8-54a3ce18847d.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvNmQ2ZDQyZWItZWJkNy00ZjVlLTkzZTAtZGYzMzAyMDcyNjgyXC9kZTBibXJ5LTFiMmQ5YjZhLTAxMzQtNGIwNy05NmE4LTU0YTNjZTE4ODQ3ZC5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.md75lFfyGp9iQf0pVEBouAyo_AExtQ8nDylYkSUCf2M")
    description = models.TextField(blank = True, default = "C'est moi, j'aime senchou")
    #profspic = models.ImageField(null = True, blank = True, upload_to = "/login/pics/" , default = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/6d6d42eb-ebd7-4f5e-93e0-df3302072682/de0bmry-1b2d9b6a-0134-4b07-96a8-54a3ce18847d.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvNmQ2ZDQyZWItZWJkNy00ZjVlLTkzZTAtZGYzMzAyMDcyNjgyXC9kZTBibXJ5LTFiMmQ5YjZhLTAxMzQtNGIwNy05NmE4LTU0YTNjZTE4ODQ3ZC5qcGcifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.md75lFfyGp9iQf0pVEBouAyo_AExtQ8nDylYkSUCf2M")

    def __str__(self):
        return f"{self.user.username}'s Profile"