from django.test import TestCase, Client
from .models import Profs
from django.contrib.auth.models import User
from django.urls import reverse, resolve
from .views import logging_in, register

# Login Function Unit Test
class loginFunctionTests(TestCase):
    def test_url_register(self):
        resp = Client().get('/account/register/')
        self.assertEquals(200, resp.status_code)
    
    def test_url_login(self):
        resp = Client().get('/account/logging_in/')
        self.assertEquals(200, resp.status_code)

class testModels(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user("marinesenchou", password="azers123az")
        self.new_user.save()

        self.new_profile = Profs.objects.create(
            user = self.new_user
        )

    def testInstances(self):
        self.assertEquals(Profs.objects.count(), 1)

    def testInstancesCorrect(self):
        self.assertEquals(Profs.objects.first().user, self.new_user)

    def testToString(self):
        self.assertIn("marinesenchou's Profile", str(self.new_user.profs))


class TestUrls(TestCase):
    def testLoginURL(self):
        loginF = resolve(reverse("login:logging_in")).func
        self.assertEquals(loginF, logging_in)
    def testRegisterURL(self):
        registerF = resolve(reverse("login:register")).func
        self.assertEquals(registerF, register)


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.login_url = reverse("login:logging_in")
        self.register_url = reverse("login:register")
        self.logout_url = reverse("login:logging_out")

    def testLoginTemplates(self):
        self.assertEquals(self.client.get(reverse("login:logging_in")).status_code, 200)
        self.assertTemplateUsed(self.client.get(reverse("login:logging_in")), "login/login.html")

    def testRegisterTemplates(self):
        self.assertEquals(self.client.get(reverse("login:register")).status_code, 200)
        self.assertTemplateUsed(self.client.get(reverse("login:register")), "login/register.html")

    def testRegLoginLogout(self):
        repondre1 = self.client.post(self.register_url, data = {
            "username" : "momosuzunene",
            "password1" : "botan123lamy",
            "password2" : "botan123lamy"
        }, follow=True)
        self.assertContains(repondre1, "momosuzunene")
        self.assertEquals(repondre1.status_code, 200)
        self.assertTemplateUsed(repondre1, "home/index.html")

        repondre2 = self.client.post(self.logout_url, data = {}, follow = True)
        self.assertContains(repondre2, "LOGIN")
        self.assertContains(repondre2, "SIGN UP")
        self.assertTemplateUsed(repondre2, "home/index.html")