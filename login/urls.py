from django.urls import path

from . import views

app_name = 'login'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('logging_in/', views.logging_in, name='logging_in'),
    path('logout/', views.logging_out, name='logging_out'),
    path("<str:username>/", views.profelz, name = "profelz"),
]
