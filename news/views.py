from django.shortcuts import render, redirect
from .forms import komentarForm, updateKasusForm
from django.http import JsonResponse
import json
import requests
from .models import komentar

# Create your views here.
# def news(request):
#     return render(request, 'news.html')

# def jumlahkasus(request):
#     return render(request, 'jumlahkasus.html')

def tambahKomentar(request):
    if request.method == 'POST':
        form = komentarForm(request.POST)
        if form.is_valid():
            inpMasuk = komentar()
            inpMasuk.komentar = form.cleaned_data['komentar']
            inpMasuk.save()
        return redirect("/news")

    else:
            inpMasuk = komentar.objects.all()
            form = komentarForm()
            response = {'inpMasuk':inpMasuk, 'form': form}
            return render(request, 'news.html', response)

def kasus(request):
    if request.method == 'POST':
        form2 = updateKasusForm(request.POST)
        return redirect("/detail")
    else:
        form2 = updateKasusForm()
        response = {'form2' : form2}
        return render(request, 'jumlahkasus.html', response)

def search(request):
    q = request.GET['q']
    
    url_tujuan2 = 'https://newsapi.org/v2/top-headlines?q=' + q + '&sortBy=popularity&country=id&category=health&apiKey=961851e1166e40b7bff978b6caa23692'
    
    req = requests.get(url_tujuan2)

    x = json.loads(req.content)

    return JsonResponse(x, safe=False)